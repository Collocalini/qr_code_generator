
{-# LANGUAGE OverloadedStrings #-}
-----------------------------------------------------------------------------
--
-- Module      :  Main
-- Copyright   :
-- License     :  AllRightsReserved
--
-- Maintainer  :
-- Stability   :
-- Portability :
--
-- |
--
-----------------------------------------------------------------------------

module Main (
    main
) where

import qualified Data.QRCode as QR
import Text.Parsec
import qualified Codec.Picture as CPic
import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString.UTF8 as Butf8
import Data.Array
import Data.Word
import System.Environment

main = do

   args<- getArgs

   let input_file = head args
   let template_file = head $ drop 1 args
   let preffix_string = unwords $ drop 2 args

   f<-readFile input_file
   q<-readFile template_file
   let fl = map (\l-> preffix_string ++ l) $ filter (not . null) $ lines f
   let fbl = map Butf8.fromString fl
   let names = map (\i-> "x" ++ show i ++ ".png") [1..]
   let svgs_names = map (\i-> "set" ++ show i ++ ".svg") [1..]


   mapM_ (\(n,l)->do
            qrencode<- QR.encodeByteString l Nothing QR.QR_ECLEVEL_M QR.QR_MODE_EIGHT True
            CPic.writePng n $ genQRimage' $ QR.toMatrix qrencode
        )
      $ zip names fbl


   let noc = map numberOfCopies fl

   mapM_ (print . numberOfCopies) fl


   let insertQRs = concatMap (\(n,c)-> replicate c n) $ zip names noc
   let insertQRcapts = concatMap (\(n,c)-> replicate c (take 30 n)) $ zip fl noc
   mapM_ (\(n,c)-> writeFile n c) $ zip svgs_names
     $ insertQRs_perFile q insertQRs insertQRcapts





qrs_per_page = 24


genQRimage' :: [[Word8]] -> CPic.Image CPic.Pixel8
genQRimage' m = g
   where

   w = length $ head m
   h = length m

   a :: Array (Int, Int) CPic.Pixel8
   a = listArray ((0,0),(w-1, h-1)) $ map c $ concat m

   g :: CPic.Image CPic.Pixel8
   g = CPic.generateImage (\x y -> a ! (x,y)) w h

   c 1 = 0
   c _ = 255


numberOfCopies :: String -> Int
numberOfCopies input = step1 $ take 2 $ reverse $ step2 $ words input
   where
   step2 []  = []
   step2 [_] = ["",""]
   step2 x   = x


   step1 [l,r]
      |r == "шт."   = read l --
      |r == "упак." = read l --
      |otherwise = 1
   step _ = 1



insertQR :: Parsec String () [String] -> String -> [String] -> String
insertQR p input qrs
   |length qrs == qrs_per_page =
      case (parse p "(unknown)" input) of
        Left e -> ""
        Right r -> std input qrs r qrs
   |length qrs < qrs_per_page =
      case (parse p "(unknown)" input) of
        Left e -> ""
        Right r -> std input qrs r $ take qrs_per_page (qrs ++ (repeat (last qrs)))
   |otherwise =
      case (parse p "(unknown)" input) of
        Left e -> ""
        Right r -> std input qrs r $ take qrs_per_page qrs

   where
   std input qrs r q = (concat (map (\(r,q)-> r ++ q) $ zip r q)) ++ (last r)



insertQRimgs_perFile _ []     = []
insertQRimgs_perFile tsvg qrs =
   (insertQRimgs tsvg $ take qrs_per_page qrs):(insertQRimgs_perFile tsvg $ drop qrs_per_page qrs)


insertQRimgs :: String -> [String] -> String
insertQRimgs input qrs = insertQR iqrimgs input qrs


insertQRcaptions_perFile _ []     = []
insertQRcaptions_perFile tsvg c =
   (insertQRcaptions tsvg $ take qrs_per_page c):(insertQRcaptions_perFile tsvg $ drop qrs_per_page c)


insertQRs_perFile _ [] _    = []
insertQRs_perFile tsvg qrs c =
   (cc $ qq tsvg):
   (insertQRs_perFile tsvg (drop qrs_per_page qrs) (drop qrs_per_page c))
   where
   cc t = insertQRcaptions t $ take qrs_per_page c
   qq t = insertQRimgs     t $ take qrs_per_page qrs



insertQRcaptions :: String -> [String] -> String
insertQRcaptions input qrs = insertQR iqrCaptions input qrs

qr_code_image = string $ "${qr_code_image}"
qr_code_caption = string $ "${qr_code_caption}"

iqrimg = do
   x1<-manyTill anyChar (try $ qr_code_image)
   return (x1)

iqrimgs =do
   x<-count qrs_per_page iqrimg
   y<-manyTill anyChar eof
   return (x ++ [y])


iqrCaption = do
   x1<-manyTill anyChar (try $ qr_code_caption)
   return (x1)

iqrCaptions =do
   x<-count qrs_per_page iqrCaption
   y<-manyTill anyChar eof
   return (x ++ [y])





